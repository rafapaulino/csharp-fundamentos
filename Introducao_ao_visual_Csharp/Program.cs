﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Introducao_ao_visual_Csharp
{
    class Program
    {
        static void Main(string[] args)
        {
            string meuNome = "Rafael";
            //exibe a variavel na tela
            Console.WriteLine("Meu nome: "+meuNome);

            /*
                Comentários em bloco
            */

            decimal meuSalario = 3.10m;
            DateTime meuAniversario = DateTime.Now.Date;
            char meuSexo = 'M';
            bool possuiFerias = true;

            Console.WriteLine("Salário: " + meuSalario);
            Console.WriteLine("Aniversário: " + meuAniversario);
            Console.WriteLine("Sexo: " + meuSexo);
            Console.WriteLine("Possuí Férias: " + possuiFerias);

            string tudoJunto = string.Format("Nome:{0}\nSalário:{1}\nSexo:{2}\n",meuNome,meuSalario,meuSexo);
            Console.WriteLine(tudoJunto);

            operadoresAritimeticos();

            bool ePar = oNumeroEPar(2);

            Console.WriteLine("É Par? "+ePar);

            int multiplicacao = 2 + 3 * 4;

            Console.WriteLine("Multiplicação: " + multiplicacao);

            incrementoDecremento();

            decimal primeiroNumero = 10;
            decimal segundoNumero = 10;

            var calculadora = new Calculadora(primeiroNumero,segundoNumero);
            var adicao =  calculadora.Somar();
            var multiplicar = calculadora.Multiplicar();
            var subtracao = calculadora.Subtrair();
            var divisao = calculadora.Dividir();

            Console.WriteLine("Adição: " + adicao);
            Console.WriteLine("Multiplicação: " + multiplicar);
            Console.WriteLine("Subtração: " +subtracao);
            Console.WriteLine("Divisão: " + divisao);

            //abre uma janela do DOS
            Console.ReadKey();

        }

        static void operadoresAritimeticos()
        {
            int meuPrimeiroNumero = 10;
            int meuSegundoNumero = 20;
            int meuTerceiroNumero = 30;

            int resultadoAdicao = meuPrimeiroNumero + meuSegundoNumero + meuTerceiroNumero;

            int retornoSubtracao = resultadoAdicao - meuPrimeiroNumero;

            int retornoMultiplicacao = resultadoAdicao * retornoSubtracao;

            int retornoDivisao = retornoMultiplicacao / meuPrimeiroNumero;

            Console.WriteLine("Retorno Adição: "+resultadoAdicao);
            Console.WriteLine("Retorno Subtração: " + retornoSubtracao);
            Console.WriteLine("Retorno Multiplicação: " + retornoMultiplicacao);
            Console.WriteLine("Retorno Divisão: " + retornoDivisao);
        }

        static bool oNumeroEPar(int numero)
        {
            if (numero % 2 == 0)
            {
                return true;
            }

            return false;
        }

        static void incrementoDecremento()
        {
            int saldo = 0;
            saldo++;
            Console.WriteLine("Saldo: " + saldo);

        }
    }
}
