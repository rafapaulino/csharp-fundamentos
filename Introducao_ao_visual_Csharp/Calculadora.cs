﻿namespace Introducao_ao_visual_Csharp
{
    public class Calculadora
    {
        private decimal _primeiroNumero;
        private decimal _segundoNumero;

        public Calculadora(decimal primeiroNumero, decimal segundoNumero)
        {
            this._primeiroNumero = primeiroNumero;
            this._segundoNumero = segundoNumero;
        }

        public decimal Somar()
        {
            return this._primeiroNumero + this._segundoNumero;
        }

        public decimal Multiplicar()
        {
            return this._primeiroNumero * this._segundoNumero;
        }

        public decimal Subtrair()
        {
            return this._primeiroNumero - this._segundoNumero;
        }

        public decimal Dividir()
        {
            return this._primeiroNumero / this._segundoNumero;
        }
    }
}