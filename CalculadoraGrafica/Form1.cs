﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Introducao_ao_visual_Csharp;

namespace CalculadoraGrafica
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textPrimeiroNumero_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            try
            {

                if (textPrimeiroNumero.Text != "" && textSegundoNumero.Text != "")
                {
                    var primeiroNumero = Convert.ToDecimal(textPrimeiroNumero.Text);
                    var segundoNumero = Convert.ToDecimal(textSegundoNumero.Text);

                    var calculadora = new Calculadora(primeiroNumero, segundoNumero);

                    labelResultado.Text = Convert.ToString(calculadora.Somar());
                }
                else
                {
                    MessageBox.Show("Informe o primeiro numero.");
                }
            }
            catch (Exception erro)
            {
                MessageBox.Show(erro.Message);
            }

        }
    }
}
